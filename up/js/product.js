const slides = document.querySelector(".product__slideshow--sliders")
  .children;
const indicatorImgs = document.querySelector(
  ".product__slideshow--indicators"
).children;

// console.log(indicatorImgs[0]);
// console.log(indicatorImgs[2]);

for (let i = 0; i < indicatorImgs.length; i++) {
  indicatorImgs[i].addEventListener("click", function () {
    

    // Getting The Slide Imgas
    for (let j = 0; j < indicatorImgs.length; j++) {
      indicatorImgs[j].classList.remove("active");
    }

    this.classList.add("active");

    // getting the nex slide
    const id = this.getAttribute("data-id");
    for (let k = 0; k < slides.length; k++) {
      slides[k].classList.remove("active");
    }

    slides[id].classList.add("active");
  });
}